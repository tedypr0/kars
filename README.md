# Kars

# Project Description
**Kars** is an online platform for selling/buying vehicles.

# Functional Requirements
## Entities
- Each **user** must have a profile picture, first and last name, email, password and a phone number.
  - The email will serve as their username so it must be a valid email and unique in the application.
  - The password must be at least 8 symbols and should contain capital letter, digit, and special symbol.
  - The first and last names must be between 2 and 20 characters long.
  - The phone number must be between 10 and 20 characters long containing only numbers.
- Each **car** must have atleast 2 pictures, model, brand, hp, year, comment about the vehicle and **more**
## Public Section
The public part must be visible without authentication. 

Anonymous users must be able to register.

The anonymous users must also be able to browse the catalog of available cars for sale. 
The user must be able to filter the cars by brand, model, horse power (hp and kw), year, ***more***, as well as to sort the cars by price and/or year(asc,desc) **perhaps more in the future**.

## Private Part (Students)
Must be accessible only if the student is authenticated.
### Users
Users **must** have private section (profile page).

Users must be able to:

- View and edit their profile (names and picture).
- Change their password.
- Add cars to wishlist.

## Private Part (Moderators) 
**Maybe in the future**

## Administration Part
Administrators must have access to all the functionality that users do.

Administrators must not be registered though the ordinary registration process. They will be accepted or denied by other administrators upon interview.

They must be able to modify/delete vehicles and users.
## Optional features
**Email Verification** – In order for the registration to be completed, the user must verify their email by clicking on a link sent to their email by the application. Before verifying their email, users cannot make transactions.

**Refer a Friend** – A user can enter the email of people, not yet registered for the application, and invite them to register. The application sends to that email a registration link.

**Car advertisement confirmation** – the user can receive an email confirmation after successfully creating an ad of his car with all essential information regarding the car.

**Easter eggs** – Creativity is always welcome and appreciated. Find a way to add something fun and/or interesting, maybe an Easter egg or two to you project to add some variety.

**More in the future**
## REST API
To provide other developers with your service, you need to develop a REST API. It should leverage HTTP as a transport protocol and clear text JSON for the request and response payloads.

A great API is nothing without a great documentation. The documentation holds the information that is required to successfully consume and integrate with an API. You must use [Swagger](https://swagger.io/) to document yours.

The REST API provides the following capabilities: 

1. Users
   1. CRUD operations (must)
   1. Enroll to courses (must)
   1. Search by first, last name or email (must)
1. Cars
   1. CRUD operations (must)

# Technical Requirements
## General
- Follow [OOP](https://en.wikipedia.org/wiki/Object-oriented_programming) principles when coding
- Follow [KISS](https://en.wikipedia.org/wiki/KISS_principle), [SOLID](https://en.wikipedia.org/wiki/SOLID), [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) principles when coding
- Follow REST API design [best practices](https://blog.florimondmanca.com/restful-api-design-13-best-practices-to-make-your-users-happy) when designing the REST API (see Appendix)
- Use tiered project structure (separate the application in layers)
- The service layer (i.e., "business" functionality) must have at least 80% unit test code coverage
- Follow [BDD](https://en.wikipedia.org/wiki/Behavior-driven_development) when writing unit tests
- You should implement proper exception handling and propagation
- Try to think ahead. When developing something, think – “How hard would it be to change/modify this later?”

## Database
The data of the application must be stored in a relational database. You need to identify the core domain objects and model their relationships accordingly. Database structure should avoid data duplication and empty data (normalize your database).

Your repository must include two scripts – one to create the database and one to fill it with data.
## Git
Commits in the GitLab repository should give a good overview of how the project was developed, which features were created first and the people who contributed. Contributions from all team members must be evident through the git commit history! The repository must contain the complete application source code and any scripts (database scripts, for example).

Provide a link to a GitLab repository with the following information in the README.md file:

- Project description
- Link to the Swagger documentation (must)
- Link to the hosted project (if hosted online)
- Instructions how to setup and run the project locally 
- Images of the database relations (must)

## Optional Requirements
Besides all requirements marked as should and could, here are some more *optional* requirements:

- Use a branching while working with Git.
- Integrate your app with a Continuous Integration server (e.g., GitLab’s own) and configure your unit tests to run on each commit to the master branch.
- Host your application’s backend in a public hosting provider of your choice (e.g., AWS, Azure, Heroku).

# Appendix
- [Guidelines for designing good REST API](https://blog.florimondmanca.com/restful-api-design-13-best-practices-to-make-your-users-happy)
- [](https://blog.florimondmanca.com/restful-api-design-13-best-practices-to-make-your-users-happy)[Guidelines for URL encoding](http://www.talisman.org/~erlkonig/misc/lunatech%5Ewhat-every-webdev-must-know-about-url-encoding/)
- [](http://www.talisman.org/~erlkonig/misc/lunatech%5Ewhat-every-webdev-must-know-about-url-encoding/)[Always prefer constructor injection](https://www.vojtechruzicka.com/field-dependency-injection-considered-harmful/)
- [](https://www.vojtechruzicka.com/field-dependency-injection-considered-harmful/)[Git commits - an effective style guide](https://dev.to/pavlosisaris/git-commits-an-effective-style-guide-2kkn)
- [](https://dev.to/pavlosisaris/git-commits-an-effective-style-guide-2kkn)[How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)
# Legend
- Must – Implement these first.
- Should – if you have time left, try to implement these.
- Could – only if you are ready with everything else give these a go.
# Goals
- Read about https and what does it require to make it work.
- Save images to a cloud.
- Encrypt passwords.
- Email verification upon registration.
- Forgotten passsword.
- Develop a mobile version.
