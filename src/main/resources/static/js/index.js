var min = 1960,
    max = 2022,
    select = document.getElementById('dropdownYear');
for (var i = max; i>=min; i--){
    var opt = document.createElement("option");
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}

var min = 5000, 
    max = 200000,
    addDots= 0,
    select = document.getElementById("kilometers");
    var option = document.createElement("option");
    for(var i = min; i<=max; i+=5000){
        option = document.createElement("option");
        option.value = i;
        addDots = i.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        option.innerHTML = addDots + " km";
        select.appendChild(option);
    }

var min = 500,
    max = 100000,
    selectPrice = document.getElementById("price-selection");
var option;
    var adder = 500;
    var isIn = false;
    for(var i = min; i<=max; i+=adder){
    
        //maybe if
        addDots = i.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        if(i>=15000 && !isIn){
            adder=5000;
            isIn =true;
        }
        optionPrice = document.createElement("option");
        optionPrice.value = i;
        optionPrice.innerHTML = addDots + " BGN";
        selectPrice.appendChild(optionPrice);
    }

    var input = document.getElementById("price-input");
    var selection = document.getElementById("price-selection");

    function displaySelection(){
        if(selection.value === "default"){
            input.value = "Всички";
            
        }else{
       input.value = (selection.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")) + " BGN";
       input.innerHTML = selection.innerHTML;      
        }

       selection.value = "";      
    }

    function addFormat(){
            input.value = input.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " BGN";
    }

    function removeFormat(){       
        input.value = input.value.toString().replace(/\D/g,'');    
    }

    function isBrandSelected(){
        var brand = document.getElementById("brand-options");
        var model = document.getElementById("model-options");
        if(brand.value!=="default"){
            model.removeAttribute("disabled");
        }else{
            model.setAttribute("disabled","");
            $("#model-options").val("default");
        }
    }