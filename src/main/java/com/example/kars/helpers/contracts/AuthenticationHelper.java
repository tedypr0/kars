package com.example.kars.helpers.contracts;

import com.example.kars.models.User;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpSession;

public interface AuthenticationHelper {
    User tryGetUser(HttpHeaders headers);
    User tryGetUser(HttpSession session);
    User verifyAuthentication(String username, String password);
}
