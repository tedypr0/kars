package com.example.kars.services.contracts;

import com.example.kars.models.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getById(int userId);

    User getByEmail(String email);

    User create(User user);

    User update(User user);

    User delete(User user);
}
