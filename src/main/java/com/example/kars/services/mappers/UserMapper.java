package com.example.kars.services.mappers;

import com.example.kars.models.User;
import com.example.kars.models.dtos.RegisterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    @Autowired
    public UserMapper() {}

    public User dtoToObj(RegisterDto registerDto){
        User user = new User();
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setPassword(registerDto.getPassword());
        user.setPhoneNumber(registerDto.getPhoneNumber());
        user.setRole_id(1);
        return user;
    }
}
