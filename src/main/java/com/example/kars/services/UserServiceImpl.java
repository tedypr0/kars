package com.example.kars.services;

import com.example.kars.exceptions.DuplicateEntityException;
import com.example.kars.exceptions.EntityNotFoundException;
import com.example.kars.models.User;
import com.example.kars.repositories.contracts.UserRepository;
import com.example.kars.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.parser.Entity;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int userId) {
        return userRepository.getById(userId);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public User create(User user) {
        boolean userExists = true;
        try {
            userRepository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            userExists = false;
        }
        if (userExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        return userRepository.create(user);
    }

    @Override
    public User update(User user) {
        userRepository.getByEmail(user.getEmail());
        return userRepository.update(user);
    }

    @Override
    public User delete(User user) {
        userRepository.getByEmail(user.getEmail());
        return userRepository.delete(user.getId());
    }
}
