package com.example.kars.repositories.contracts;

import com.example.kars.models.User;

import java.util.List;

public interface UserRepository {

    List<User> getAll();

    User getById(int userId);

    User getByEmail(String email);

    User create(User user);

    User update(User user);

    User delete(int userId);

}
