package com.example.kars.repositories;

import com.example.kars.exceptions.EntityNotFoundException;
import com.example.kars.models.User;
import com.example.kars.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.getResultList();
        }
    }

    @Override
    public User getById(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where id=:id", User.class);
            query.setParameter("id", userId);
            User user = query.getSingleResult();
            if (user == null) {
                throw new EntityNotFoundException("User", "id", userId);
            }
            return user;
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email=:email", User.class);
            query.setParameter("email", email);
            User user = query.getSingleResult();
            if (user == null) {
                throw new EntityNotFoundException("User", "email", email);
            }
            return user;
        }
    }

    @Override
    public User create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
        return user;
    }

    @Override
    public User update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public User delete(int userId) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, userId);
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
            return user;
        }
    }
}
